#include <sourcemod>
#include <clientprefs>
#include <cstrike>
#include <warmod>
#undef REQUIRE_PLUGIN
#include <updater>

// CVars' handles
new Handle:cvar_save_scores = INVALID_HANDLE;
new Handle:cvar_startmoney = INVALID_HANDLE;
new Handle:g_h_chat_prefix = INVALID_HANDLE;

// Cvars' variables
new bool:save_scores = true;
new g_CSDefaultCash = 800;
new String:CHAT_PREFIX[64];

/* switches */
new bool:g_live = false;
new bool:g_first_half = true;
//new bool:g_overtime = false;

// DB handle
new Handle:g_hDB = INVALID_HANDLE;

// Other stuff
new bool:justConnected[MAXPLAYERS+1] = {true, ...};
new bool:g_NextRoundNewGame = false;

// Players info
new g_iPlayerFrags[MAXPLAYERS+1];
new g_iPlayerDeaths[MAXPLAYERS+1];
new g_iPlayerCash[MAXPLAYERS+1];
new g_iPlayerAssists[MAXPLAYERS+1];
new g_iPlayerRealScore[MAXPLAYERS+1];
new String:g_iPlayerSteamID[MAXPLAYERS+1][30];
new g_iPlayerTeam[MAXPLAYERS+1];
new g_iPlayerAlive[MAXPLAYERS+1];

//Round info
new g_t_win_streak = 0;
new g_ct_win_streak = 0;
new g_bomb_planted = false;

// Regular plugin information
#define UPDATE_SAVESCORES	"http://warmod.bitbucket.org/savescores.txt"
//http://www.fnatic.com/content/95534
#define PLUGIN_VERSION "0.0.1"

public Plugin:myinfo = 
{
	name = "Warmod SaveScores [BFG]",
	author = "Versatile_BFG",
	description = "This is a cut-down and modified version of savescores by exvel. Made for Warmod BFG",
	version = PLUGIN_VERSION,
	url = "www.sourcemod.net"
}

public OnPluginStart()
{
	if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_SAVESCORES);
    }

	// Creating cvars
	CreateConVar("wm_save_scores_version", PLUGIN_VERSION, "Warmod Save Scores Version", FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	cvar_save_scores = CreateConVar("wm_save_scores", "1", "Enabled/Disabled warmod save scores functionality, 0 = off/1 = on", FCVAR_PLUGIN, true, 0.0, true, 1.0);
	g_h_chat_prefix = CreateConVar("wm_chat_prefix", "WarMod_BFG", "Change the chat prefix. Default is WarMod_BFG", FCVAR_PROTECTED);
	cvar_startmoney = FindConVar("mp_startmoney");
	
	// Hooking cvar change
	HookConVarChange(cvar_save_scores, OnCVarChange);
	HookConVarChange(cvar_startmoney, OnCVarChange);
	HookConVarChange(g_h_chat_prefix, OnCVarChange);
	
	// Hooking event
	HookEvent("player_team", Event_PlayerTeam);
	HookEvent("player_disconnect", Event_PlayerDisconnect);
	HookEvent("round_freeze_end", Event_Round_Freeze_End);
	HookEvent("round_start", Event_NewGameStart);
	HookEvent("round_end", Event_RoundEnd);
	HookEvent("player_death", Event_Player_Death);
	HookEvent("bomb_planted", Event_Bomb_Planted);
	HookEvent("bomb_defused", Event_Bomb_Defused);
	HookEvent("item_purchase", Event_Item_Purchase);
	
	// Creating commands
	RegAdminCmd("sm_save_scores_reset", Command_Clear, ADMFLAG_GENERIC, "Resets all saved scores");
	
	// Other regular stuff
	LoadTranslations("wm_savescores.phrases");
//	AutoExecConfig(true, "wm_savescores");
	
	// Finding offset for CS cash
	g_iAccount = FindSendPropOffs("CCSPlayer", "m_iAccount");
	if (g_iAccount == -1)
	{
		SetFailState("m_iAccount offset not found");
	}
	
	// Creating DB...
	InitDB();
	
	// ...and clear it
	ClearDB();
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	RegPluginLibrary("wm_savescores");
	CreateNative("Set_StartMoney", Native_Set_StartMoney);

	return APLRes_Success;
}

public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_SAVESCORES);
    }
}

public Event_Player_Death(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return;
	}
	
	new client = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
	{
		Event_Score_Update(client);
	}
}

public Event_Item_Purchase(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return;
	}
	
	new client = GetClientOfUserId(GetEventInt(event, "userid"));	
	
	if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
	{
		g_iPlayerCash[client] = GetCash(client);
	}
}

public Event_Bomb_Defused(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return;
	}
	
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
	{
		Event_Score_Update(client);
	}
}

public Event_Bomb_Planted(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return;
	}
	
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
	{
		Event_Score_Update(client);
	}
	g_bomb_planted = true;
}

public Event_Score_Update(client)
{
	if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
	{
		GetClientAuthString(client, g_iPlayerSteamID[client], sizeof(g_iPlayerSteamID[]));
		g_iPlayerFrags[client] = GetFrags(client);
		g_iPlayerDeaths[client] = GetDeaths(client);
		g_iPlayerCash[client] = GetCash(client);
		g_iPlayerAssists[client] = GetAssists(client);
		g_iPlayerRealScore[client] = GetRealScore(client);
	}
}

// If round is a new game we should clear DB or restore players' scores
public Action:Event_NewGameStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return Plugin_Continue;
	}
	
	if (g_NextRoundNewGame)
	{
		g_NextRoundNewGame = false;
	
		ClearDB();
	
		for (new client = 1; client <= MaxClients; client++)
		{
			if (IsClientInGame(client) && !IsFakeClient(client) && !justConnected[client])
			{
				SetFrags(client, g_iPlayerFrags[client]);
				SetDeaths(client, g_iPlayerDeaths[client]);
				SetCash(client, g_iPlayerCash[client]);
				SetAssists(client, g_iPlayerAssists[client]);
				SetRealScore(client, g_iPlayerRealScore[client]);
			}
		}
	}
	return Plugin_Continue;	
}

public Action:Event_Round_Freeze_End(Handle:event, const String:name[], bool:dontBroadcast)
{
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
		{
			g_iPlayerTeam[client] = GetClientTeam(client);
			if (g_iPlayerTeam[client] == 2 || g_iPlayerTeam[client] == 3)
			{
				g_iPlayerAlive[client] = true;
			}
			else
			{
				g_iPlayerAlive[client] = false;
			}
		}
	}

}

// Here we will also mark next round as a new game if needed
public Action:Event_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return Plugin_Continue;
	}
	
	decl String:szMessage[32];
	GetEventString(event, "message", szMessage, sizeof(szMessage));
	
	if (StrEqual(szMessage, "#Game_Commencing") || StrEqual(szMessage, "#Round_Draw"))
	{
		g_NextRoundNewGame = true;
		return Plugin_Continue;
	}
	
	new reason = GetEventInt(event, "reason");
	new winner = GetEventInt(event, "winner");
	new roundCashT = 0;
	new roundCashCT = 0;
	
	if (winner == 2)
	{
		g_t_win_streak++;
		g_ct_win_streak = 0;
		
		if (g_t_win_streak == 1)
		{
			roundCashCT = 1400;
		}
		else if (g_t_win_streak == 2)
		{
			roundCashCT = 1900;
		}
		else if (g_t_win_streak == 3)
		{
			roundCashCT = 2400;
		}
		else if (g_t_win_streak == 4)
		{
			roundCashCT = 2900;
		}
		else
		{
			roundCashCT = 3400;
		}
		//T win [Bomb detonation]
		if (reason == 0)
		{
			roundCashT = 3500;
		}
		//T win [Elimination]
		else if (reason == 8)
		{
			roundCashT = 3250;
		}
	}
	else if (winner == 3)
	{
		g_ct_win_streak++;
		g_t_win_streak = 0;
		
		if (g_ct_win_streak == 1)
		{
			roundCashT = 1400;
		}
		else if (g_ct_win_streak == 2)
		{
			roundCashT = 1900;
		}
		else if (g_ct_win_streak == 3)
		{
			roundCashT = 2400;
		}
		else if (g_ct_win_streak == 4)
		{
			roundCashT = 2900;
		}
		else
		{
			roundCashT = 3400;
		}
		//CT win [Bomb defused]
		if (reason == 6)
		{
			if (g_bomb_planted)
			{
				roundCashT = roundCashT + 800;
			}
			roundCashCT = 3500;
		}
		//CT win [Elimination]
		else if (reason == 7)
		{
			roundCashCT = 3250;
		}
		//CT win [Out of time]
		else if (reason == 11)
		{
			roundCashCT = 3250;
		}
	}
	//Set the cash for the client that dropped
	for (new client = 1; client <= MaxClients; client++)
	{
		if (g_iPlayerAlive[client])
		{
			if (!IsClientInGame(client) || GetClientTeam(client) == 0 || GetClientTeam(client) == 1)
			{
				if (g_iPlayerTeam[client] == 2)
				{
					g_iPlayerCash[client] = g_iPlayerCash[client] + roundCashT;
				}
				else if (g_iPlayerTeam[client] == 3)
				{
					g_iPlayerCash[client] = g_iPlayerCash[client] + roundCashCT;
				}
				decl String:query[200];
				decl String:steamId[30];
				new set_money = g_iPlayerCash[client];
				Format(steamId, sizeof(steamId), "%s", g_iPlayerSteamID[client]);
				Format(query, sizeof(query), "UPDATE savescores_scores SET money = '%i' WHERE steamid = '%s';", set_money, steamId);
				SQL_TQuery(g_hDB, EmptySQLCallback, query);
				g_iPlayerAlive[client] = false;
			}
		}
	}
	g_bomb_planted = false;
	return Plugin_Continue;
}

public OnConfigsExecuted()
{
	GetCVars();
}

// Here we are creating SQL DB
public InitDB()
{
	// SQL DB
	new String:error[255];
	g_hDB = SQLite_UseDatabase("savescores", error, sizeof(error));
	
	if (g_hDB == INVALID_HANDLE)
		SetFailState("SQL error: %s", error);
	
	SQL_LockDatabase(g_hDB);
	SQL_FastQuery(g_hDB, "VACUUM");
	SQL_FastQuery(g_hDB, "CREATE TABLE IF NOT EXISTS savescores_scores (steamid TEXT PRIMARY KEY, frags SMALLINT, deaths SMALLINT, money SMALLINT, assists SMALLINT, realscore SMALLINT, timestamp INTEGER);");
	SQL_UnlockDatabase(g_hDB);
}

// Admin command that clears all player's scores
public Action:Command_Clear(admin, args)
{
	if (!save_scores)
	{
		ReplyToCommand(admin, "Save Scores is currently disabled");
		return Plugin_Handled;
	}
	
	ClearDB();
	
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client))
		{
			SetFrags(client, 0);
			SetDeaths(client, 0);
			SetCash(client, g_CSDefaultCash);
			SetAssists(client, 0);
			SetRealScore(client, 0);
		}
	}
	
	ReplyToCommand(admin, "Players scores has been reset");
	
	return Plugin_Handled;
}

// Just clear DB. Sometimes we need it to be delayed.
ClearDB()
{
	ClearDBQuery();
}

// Doing clearing stuff
ClearDBQuery()
{
	// Clearing SQL DB
	SQL_LockDatabase(g_hDB);
	SQL_FastQuery(g_hDB, "DELETE FROM savescores_scores;");
	SQL_UnlockDatabase(g_hDB);
}

public OnMapStart()
{
	// Clear DB
	ClearDB();
}

// Syncronize DB with score varibles
public SyncDB()
{
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client) && !IsFakeClient(client) && IsClientAuthorized(client))
		{
			decl String:steamId[30];
			decl String:query[200];
			
			GetClientAuthString(client, steamId, sizeof(steamId));
			new frags = GetFrags(client);
			new deaths = GetDeaths(client);
			new cash = GetCash(client);
			new assists = GetAssists(client);
			new realscore = GetRealScore(client);
			
			Format(query, sizeof(query), "INSERT OR REPLACE INTO savescores_scores VALUES ('%s', %d, %d, %d, %d, %d, %d);", steamId, frags, deaths, cash, assists, realscore, GetTime());
			SQL_FastQuery(g_hDB, query);
		}
	}
}

// Most of the score manipulations will be done in this event
public Action:Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (!client || !save_scores || !g_live)
	{
		return Plugin_Continue;
	}
	if (IsFakeClient(client) || !IsClientInGame(client))
	{
		return Plugin_Continue;
	}
	
	// Player joined spectators and he already played so we will save his scores. Actually we need only cash but nevermind.
	if (GetEventInt(event, "team") == 1 && !justConnected[client])
	{
		InsertScoreInDB(client);
		return Plugin_Continue;
	}
	// Player returned to the team from spectators. Lets set his cash back.
	else if (GetEventInt(event, "team") > 1 && GetEventInt(event, "oldteam") < 2 && !justConnected[client])
	{
		GetScoreFromDB(client);
	}
	// Player just connected and joined team
	else if (justConnected[client] && GetEventInt(event, "team") != 1)
	{
		justConnected[client] = false;
		GetScoreFromDB(client);
	}
	
	return Plugin_Continue;
}

// Here we will put player's score into DB
public Action:Event_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!g_live || !save_scores)
	{
		return;
	}
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	justConnected[client] = true;
	
	if (!client || !save_scores || !IsClientInGame(client) || IsFakeClient(client))
	{
		return;
	}
	InsertScoreInDB(client);
}

InsertScoreInDB(client)
{
	decl String:steamId[30];
	
	GetClientAuthString(client, steamId, sizeof(steamId));
	new frags = GetFrags(client);
	new deaths = GetDeaths(client);
	new assists = GetAssists(client);
	new realscore = GetRealScore(client);
	new cash = GetCash(client);
	
	// Do not save scores if there are zero scores and default cash
	if (frags == 0 && deaths == 0 && cash == g_CSDefaultCash && assists == 0 && realscore == 0)
	{
		return;
	}

	InsertScoreQuery(steamId, frags, deaths, cash, assists, realscore);
}

InsertScoreQuery(const String:steamId[], frags, deaths, cash, assists, realscore)
{
	decl String:query[200];
	Format(query, sizeof(query), "INSERT OR REPLACE INTO savescores_scores VALUES ('%s', %d, %d, %d, %d, %d, %d);", steamId, frags, deaths, cash, assists, realscore, GetTime());
	SQL_TQuery(g_hDB, EmptySQLCallback, query);
}

// Now we need get this information back...
public GetScoreFromDB(client)
{
	if (!IsClientInGame(client))
	{
		return;
	}
	
	decl String:steamId[30];
	decl String:query[200];
	
	GetClientAuthString(client, steamId, sizeof(steamId));
	Format(query, sizeof(query), "SELECT * FROM	savescores_scores WHERE steamId = '%s';", steamId);
	SQL_TQuery(g_hDB, SetPlayerScore, query, client);
}

public OnMapEnd()
{
	if (save_scores)
	{
		SyncDB();
	}
}

// ...and set player's score and cash if needed
public SetPlayerScore(Handle:owner, Handle:hndl, const String:error[], any:client)
{
	if (hndl == INVALID_HANDLE)
	{
		LogError("SQL Error: %s", error);
		return;
	}
	
	if (SQL_GetRowCount(hndl) == 0)
	{
		return;
	}
	
	if (!IsClientInGame(client))
	{
		return;
	}
	
	new score = SQL_FetchInt(hndl,1);
	new deaths = SQL_FetchInt(hndl,2);
	new cash = SQL_FetchInt(hndl,3);	
	new assists = SQL_FetchInt(hndl,4);
	new realscore = SQL_FetchInt(hndl,5);
	SetFrags(client, score);
	SetDeaths(client, deaths);
	SetAssists(client, assists);
	SetRealScore(client, realscore);
	SetCash(client, cash);
	
	PrintToChat(client, "\x01 \x09[\x04%s\x09]\x01 %t", CHAT_PREFIX, "Score restored");
	PrintToChat(client, "\x01 \x09[\x04%s\x09]\x01 %t", CHAT_PREFIX, "Cash restored", cash);
}

// Removes player's score from DB
public RemoveScoreFromDB(client)
{
	decl String:query[200];
	decl String:steamId[30];
	
	GetClientAuthString(client, steamId, sizeof(steamId));
	Format(query, sizeof(query), "DELETE FROM savescores_scores WHERE steamId = '%s';", steamId);
	SQL_TQuery(g_hDB, EmptySQLCallback, query);
}

// Set player's money to DB
public Native_Set_StartMoney(Handle:plugin, num1)
{
	new set_money = GetNativeCell(1);
	decl String:query[200];
	for (new client = 1; client <= MaxClients; client++)
	{
		if (IsClientInGame(client) && !IsFakeClient(client) && !justConnected[client])
		{
			g_iPlayerCash[client] = set_money;
			SetCash(client, g_iPlayerCash[client]);
		}
	}
	Format(query, sizeof(query), "UPDATE savescores_scores SET money = '%i';", set_money);
	SQL_TQuery(g_hDB, EmptySQLCallback, query);
}

public EmptySQLCallback(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
		LogError("SQL Error: %s", error);
}

public OnCVarChange(Handle:convar_hndl, const String:oldValue[], const String:newValue[])
{
	GetCVars();
}

// Getting data from CVars and putting it into plugin's varibles
GetCVars()
{
	save_scores = GetConVarBool(cvar_save_scores);
	g_CSDefaultCash = GetConVarInt(cvar_startmoney);
	GetConVarString(g_h_chat_prefix, CHAT_PREFIX, sizeof(CHAT_PREFIX));
}

// Warmod Forwards
public OnLiveOn3()
{
	g_NextRoundNewGame = true;
	g_live = true;
}

public OnHalfTime()
{
	g_first_half = false;
	
	if (g_live || save_scores)
	{
		SyncDB();
	}
}

public OnEndMatch()
{
	g_live = false;
	g_first_half = true;
}

public OnResetHalf()
{
	g_live = false;
}

public OnResetMatch()
{
	g_live = false;
	g_first_half = true;
	ClearDB();
}